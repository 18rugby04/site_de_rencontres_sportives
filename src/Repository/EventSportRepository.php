<?php

namespace App\Repository;

use App\Entity\EventSport;
use App\Entity\EventSportSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventSport|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventSport|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventSport[]    findAll()
 * @method EventSport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventSportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventSport::class);
    }

    public function findLastThree()
    {
        return $this->createQueryBuilder('e')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    public function findAllEventSport($search)
    {
        // dd($search->getCodePostal());
        return $this->createQueryBuilder('e')
            // ->leftJoin('e.sport', 'sport')
            ->where('e.zipCode = :code')
            ->setParameter('code', $search->getCodePostal())
            // ->andWhere('e.sport = :sp')
            // ->setParameter('sp', $search['sport']->getSportName())
            ->orderBy('e.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    // public function findSpecificEventSport(EventSportSearch $search): Query
    // {
    //     $query = $this->findSpecificEventSport();
    //     if ($search->getCodePostal()) {
    //         $query = $query
    //             ->andWhere('e.zipCode = :codePostal')
    //             ->setParameter('codePostal', $search->getCodePostal());
    //     }
    // if ($search->getTypeSport()) {
    //     $query = $query
    //         ->andWhere('e.sport = :typeSport')
    //         ->setParameter('typeSport', $search->getTypeSport());
    // }
    // return $query->getQuery();
    // }

    // /**
    //  * @return EventSport[] Returns an array of EventSport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventSport
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
