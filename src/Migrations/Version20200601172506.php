<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200601172506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event_sport ADD creator_id INT DEFAULT NULL, CHANGE description_event description_event LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE event_sport ADD CONSTRAINT FK_5B9E5EFE61220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_5B9E5EFE61220EA6 ON event_sport (creator_id)');
        $this->addSql('ALTER TABLE home CHANGE teaser teaser LONGTEXT NOT NULL, CHANGE description_home description_home LONGTEXT NOT NULL, CHANGE whatweare whatweare LONGTEXT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // $this->addSql('ALTER TABLE event_sport DROP FOREIGN KEY FK_5B9E5EFE61220EA6');
        // $this->addSql('DROP INDEX IDX_5B9E5EFE61220EA6 ON event_sport');
        $this->addSql('ALTER TABLE event_sport DROP creator_id, CHANGE description_event description_event LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE home CHANGE teaser teaser VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description_home description_home VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE whatweare whatweare VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
