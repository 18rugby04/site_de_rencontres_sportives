<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserInfoRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserInfoRepository::class)
 */
class UserInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    // ajax fonctionnement cascade clé user

    /**
     * @ORM\Column(type="string", length=5)
     * @Assert\Regex(pattern="/^[0-9]{5}$/", message="Ceci n'est pas un code postal")
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=1, minMessage="Le nom de la ville est trop court", max=255, maxMessage="Le nom de la ville est trop long")
     */
    private $City;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="userInfos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $favoriteSport;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="userInfo", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=false)
     */
    private $users;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->City;
    }

    public function setCity(string $City): self
    {
        $this->City = $City;

        return $this;
    }

    public function getFavoriteSport(): ?Sport
    {
        return $this->favoriteSport;
    }

    public function setFavoriteSport(?Sport $favoriteSport): self
    {
        $this->favoriteSport = $favoriteSport;

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(User $users): self
    {
        $this->users = $users;

        return $this;
    }
}
